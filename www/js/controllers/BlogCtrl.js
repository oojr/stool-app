angular.module('blog.controllers', ['blog.service'])

.controller('SuperPageCtrl', function($scope, BlogService) {

   BlogService.list().then(function(resp) {
     $scope.blogs = resp.data;
   })
})

.controller('BostonCtrl', function($scope, BlogService) {

   BlogService.list().then(function(resp) {
     $scope.blogs = _.filter(resp.data, {'city': 'Boston'});
   })
})


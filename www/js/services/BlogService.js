'use strict';

angular.module('blog.service', [])

.factory('BlogService', ['$http', function($http) {
    var blogService = {};

    blogService.list = function() {
      return $http.get('js/blogs.json');
    };

    return blogService;
  }
]);
